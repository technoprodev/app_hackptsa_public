<?php
namespace app_hackptsa_public\models;

use Yii;
use yii\base\Model;
use app_hackptsa_admin\models\Pemohon;

class Login extends Model
{
    public $nomor_hp;
    public $password;
    public $rememberMe = true;

    private $_user;

    public function rules()
    {
        return [
            ['nomor_hp', 'required'],
            
            ['password', 'required'],
            ['password', 'validatePassword'],

            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'nomor_hp' => 'Nomor HP',
            'password' => 'Password',
            'rememberMe' => 'Remember Me',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if ($user = $this->getUser()) {
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        } else {
            $this->addError('nomor_hp', 'Akun dengan nomor hp tersebut tidak terdartar.');
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    protected function getUser()
    {
        if ($this->_user === null) {
            if ($pemohon = Pemohon::findOne(['nomor_hp' => $this->nomor_hp])) {
                $this->_user = UserIdentity::findOne(['id' => $pemohon->id]);
            } else $this->_user = null;
        }

        return $this->_user;
    }
}
