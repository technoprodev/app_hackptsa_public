<?php
namespace app_hackptsa_public\models;

use Yii;

/**
 * This is the model class for table "user" in Database Application (dba)
 *
 * @property integer $id
 * @property string $username
 */
class User extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //username
            [['username'], 'required'],
            [['username'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['username'], 'match', 'pattern' => '/^(?=.*[a-z])([a-z0-9._-]+)$/i', 'message' => 'Username at least contain 1 word. And only number, letter, dot, dashed and underscore allowed.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
        ];
    }
}
