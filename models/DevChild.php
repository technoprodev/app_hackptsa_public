<?php
namespace app_hackptsa_public\models;

use Yii;

/**
 * This is the model class for table "dev_child".
 *
 * @property integer $id
 * @property integer $id_dev
 * @property integer $child
 *
 * @property Dev $dev
 */
class DevChild extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'dev_child';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //id_dev
            [['id_dev'], 'required'],
            [['id_dev'], 'integer'],
            [['id_dev'], 'exist', 'skipOnError' => true, 'targetClass' => Dev::className(), 'targetAttribute' => ['id_dev' => 'id']],

            //child
            [['child'], 'required'],
            [['child'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_dev' => 'Id Dev',
            'child' => 'Child',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDev()
    {
        return $this->hasOne(Dev::className(), ['id' => 'id_dev']);
    }
}
