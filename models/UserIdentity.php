<?php
namespace app_hackptsa_public\models;

use Yii;
use app_hackptsa_admin\models\Pemohon;

/**
 * This is connector for Yii::$app->user with model Pemohon
 */
class UserIdentity extends \technosmart\models\User
{

    public function getPemohon()
    {
        return $this->hasOne(Pemohon::className(), ['id' => 'id']);
    }
}