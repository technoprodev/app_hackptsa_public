<?php
namespace app_hackptsa_public\models;

use Yii;

/**
 * This is the model class for table "dev".
 *
 * @property integer $id
 * @property string $id_combination
 * @property string $id_user_defined
 * @property string $text
 * @property integer $link
 * @property string $enum
 * @property string $set
 * @property integer $checklist
 * @property string $file
 *
 * @property DevCategoryOption $link0
 * @property DevCategory[] $devCategories
 * @property DevChild[] $devChildren
 * @property DevExtend $devExtend
 */
class Dev extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_category = [];
    public $virtual_file_upload;
    public $virtual_file_download;

    public static function tableName()
    {
        return 'dev';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //id_combination
            [['id_combination'], 'string', 'max' => 32],
            [['id_combination'], 'unique'],

            //id_user_defined
            [['id_user_defined'], 'string', 'max' => 32],
            [['id_user_defined'], 'unique'],

            //text
            [['text'], 'string', 'max' => 32],

            //link
            [['link'], 'integer'],
            [['link'], 'exist', 'skipOnError' => true, 'targetClass' => DevCategoryOption::className(), 'targetAttribute' => ['link' => 'id']],

            //enum
            [['enum'], 'string'],

            //set
            [['set'], 'safe'],

            //checklist
            [['checklist'], 'integer'],

            //file
            [['file'], 'string', 'max' => 32],
            
            //virtual_category
            [['virtual_category'], 'safe'],

            /*[['text'], 'inlineValidator'],
            [['text'], function ($attribute, $params) {
                if ($this->$attribute == 'bandung') {
                    $this->addError($attribute, 'Bandung is not allowed here. Please give another value.');
                }
            }],
            [['text'], 'required',
                'when' => function ($model) { return $model->id_user_defined != 'jakarta'; },
                'whenClient' => "function (attribute, value) { return scope.dev.id_user_defined != 'jakarta'; }",
                'message' => 'Dude, please.'
            ],*/
        ];
    }

    public function inlineValidator($attribute, $params) {
        if ($this->$attribute == 'bandung') {
            $this->addError($attribute, 'Bandung is not allowed here. Please give another value.');
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        \Yii::$app->dba->createCommand()->delete('dev_category', 'id_dev = ' . (int) $this->id)->execute();
        if (is_array($this->virtual_category))
            foreach ($this->virtual_category as $key => $val) {
                $dc = new DevCategory();
                $dc->id_dev = $this->id;
                $dc->id_dev_category_option = $val;
                $dc->save();
            }

        $this->fileUpload();
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        \Yii::$app->dba->createCommand()->delete('dev_category', 'id_dev = ' . (int) $this->id)->execute();
        \Yii::$app->dba->createCommand()->delete('dev_extend', 'id_dev = ' . (int) $this->id)->execute();
        \Yii::$app->dba->createCommand()->delete('dev_child', 'id_dev = ' . (int) $this->id)->execute();

        $fileRoot = Yii::$app->params['configurations_file']['dev-file']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->file;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        $this->virtual_category = \yii\helpers\ArrayHelper::getColumn(
            $this->getDevCategories()->asArray()->all(),
            'id_dev_category_option'
        );

        if(!empty($this->file)) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['dev-file']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_file_download = $path . '/' . $this->file;
        }
    }

    public function fileUpload()
    {
        if ($this->virtual_file_upload && $this->validate()) {
            $uploadRoot = Yii::$app->params['configurations_file']['dev-file']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_file_upload->saveAs($path . '/' . $this->file);

            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_combination' => 'Id Combination',
            'id_user_defined' => 'Id User Defined',
            'text' => 'Text',
            'link' => 'Link',
            'enum' => 'Enum',
            'set' => 'Set',
            'checklist' => 'Checklist',
            'file' => 'File',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink0()
    {
        return $this->hasOne(DevCategoryOption::className(), ['id' => 'link']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevCategories()
    {
        return $this->hasMany(DevCategory::className(), ['id_dev' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevChildren()
    {
        return $this->hasMany(DevChild::className(), ['id_dev' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevExtend()
    {
        return $this->hasOne(DevExtend::className(), ['id_dev' => 'id']);
    }
}
