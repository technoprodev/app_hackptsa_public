<?php
namespace app_hackptsa_public\models;

use Yii;
use yii\base\Model;
use app_hackptsa_admin\models\Pemohon;

class Signup extends Model
{
    public $nik;
    public $nama;
    public $nomor_hp;
    public $password;
    public $password_repeat;

    public function rules()
    {
        return [
            [['nik'], 'trim', 'when' => function($model) {
                return $model->nik != NULL;
            }],
            [['nik'], 'required'],
            [['nik'], 'string', 'max' => 16],
            [['nik'], 'unique', 'targetClass' => '\app_hackptsa_admin\models\Pemohon', 'message' => 'This nik has already been taken.'],

            [['nama'], 'trim', 'when' => function($model) {
                return $model->nama != NULL;
            }],
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            [['nomor_hp'], 'trim', 'when' => function($model) {
                return $model->nomor_hp != NULL;
            }],
            [['nomor_hp'], 'required'],
            [['nomor_hp'], 'string', 'max' => 16],
            [['nomor_hp'], 'unique', 'targetClass' => '\app_hackptsa_admin\models\Pemohon', 'message' => 'This nomor hp address has already been taken.'],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>'Passwords do not match'],
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new UserIdentity();
        $user->status = 0;
        $user->name = $this->nama;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        $pemohon = new Pemohon();
        $pemohon->is_verified = 0;
        $pemohon->nik = $this->nik;
        $pemohon->nama = $this->nama;
        $pemohon->nomor_hp = $this->nomor_hp;

        // $user->validate();
        // $pemohon->validate();

        // Yii::$app->d->dd($user->errors);
        // Yii::$app->d->ddx($pemohon->errors);

        if ($user->save()) {
            $pemohon->id = $user->id;
            if ($pemohon->save()) {
                return $user;
            } else return null;
        } else return null;
    }

    public function attributeLabels()
    {
        return [
            'nik' => 'NIK',
            'nama' => 'Nama',
            'nomor_hp' => 'Nomor HP',
            'password' => 'Password',
            'password_repeat' => 'Re-enter Password',
        ];
    }
}
