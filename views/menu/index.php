<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Menu';
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

function menu_tree($list, $parent = null) {
    $html = null;

    foreach ($list as $menu) {
        // parent has child
        if ($menu["parent"] == $parent) {
            if (!isset($menu['enable']))
                continue;
            
            $html .= "<li ".($menu['enable'] ? null : "class='text-muted'")." data-id='$menu[id]'>
                        <i class='glyphicon glyphicon-th sortable-handle'></i> 
                        <span>$menu[title]</span>

                        <div class='pull-right'>"
                            .(Yii::$app->controller->can('update') ? 
                                Html::a( // status
                                    $menu['enable'] ? "Aktif" : "Non-Aktif", 
                                    ["status", "id"=>$menu["id"]],
                                    [
                                        "class"=>[$menu['enable'] ? "label label-success" : "label label-danger"],
                                        "data-method" => "POST",
                                    ]
                                ) : 
                                "<span class='label ".($menu['enable'] ? "label-success" : "label-danger")."'>"
                                    .($menu['enable'] ? "Aktif" : "Non-Aktif")
                                ."</span>"
                            )
                        ."</div>
                        <div class='pull-right parent-hover'>"
                            .(Yii::$app->controller->can('create') ? Html::a( // create
                                "<i class='glyphicon glyphicon-plus'></i> Tambah", 
                                ["create", "parent"=>$menu["id"]],
                                ["modal"=>""]
                            ) : "")
                            .(Yii::$app->controller->can('update') ? Html::a( // update
                                "<i class='glyphicon glyphicon-edit'></i> Edit", 
                                ["update", "id"=>$menu["id"]],
                                ["modal"=>""]
                            ) : "")
                            .(Yii::$app->controller->can('delete') ? Html::a( // delete
                                "<i class='glyphicon glyphicon-remove'></i> Hapus", 
                                ["delete", "id"=>$menu["id"]],
                                [
                                    "data"=> [
                                        "confirm"=>"",
                                        "method"=>"post",
                                    ]
                                ]
                            ) : "")
                        ."</div> <div class='clearfix'></div>"
                        .menu_tree($list, $menu['id'])
                    ."</li>";
        }
    }

    return "<ul>$html</ul>";
}

?>

<div class="menu-index">
    <?php if (Yii::$app->controller->can('create')) : ?>
        <p> <?= Html::a(
            '<span><i class="fa fa-plus bigger-110 blue"> Tambah menu</i><span class="hidden">Tambah menu</span></span>', 
            ['create'],
            ['class' => 'dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold'],
            ['modal'=>'']) ?>
        </p>
    <?php endif; ?>

    <div id="sortable"> 
        <?= menu_tree($query->queryAll()) ?> 
        <div class='loading'><?= Html::img("@web/images/loading.gif") ?></div>
    </div>
</div>