<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['permohonan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['permohonan'], ['class' => '']);
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?= $this->render('/layouts/page-header', [
    'title' => $title,
    'icon' => null,
    'backLink' => 'site/index',
]) ?>

<div class="container padding-y-30">

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nomor_permohonan'] ?></label>
        <?= Html::activeHiddenInput($model['permohonan'], 'nomor_permohonan'); ?>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nomor_permohonan ?></p>
    </div>

    <div class="form-group">
        <label class="control-label">NIK</label>
        <p class="form-control-static border-bottom"><?= Yii::$app->user->identity->pemohon->nik ?></p>
    </div>

    <div class="form-group">
        <label class="control-label">Nama</label>
        <p class="form-control-static border-bottom"><?= Yii::$app->user->identity->pemohon->nama ?></p>
    </div>

    <div class="form-group">
        <label class="control-label">No HP</label>
        <p class="form-control-static border-bottom"><?= Yii::$app->user->identity->pemohon->nomor_hp ?></p>
    </div>

    <?= $form->field($model['permohonan'], 'keperluan')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'keperluan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['permohonan'], 'keperluan', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'keperluan', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'keperluan')->end(); ?>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

</div>