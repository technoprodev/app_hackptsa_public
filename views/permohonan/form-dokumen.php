<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
$permohonanDokumens = [];
if (isset($model['permohonan_dokumen']))
    foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen) {
        $pd = $permohonanDokumen->attributes;
        $pd['virtual_nama_file_upload'] = $permohonanDokumen->virtual_nama_file_upload;
        $pd['virtual_nama_file_download'] = $permohonanDokumen->virtual_nama_file_download;
        $permohonanDokumens[] = $permohonanDokumen->attributes;
    }

$jenisPermintaanDoks = [];
$jpds = \app_hackptsa_admin\models\JenisPermintaanDok::find()->with('jenisBerkases')->all();
foreach ($jpds as $key => $jenisPermintaanDok) {
    $jenisBerkases = [];
    $jbs = $jenisPermintaanDok->jenisBerkases;
    foreach ($jbs as $key => $jenisBerkas) {
        $jb = $jenisBerkas->attributes;
        $jenisBerkases[] = $jb;
    }

    $temp = $jenisPermintaanDok->attributes;
    $temp['jenisBerkases'] = $jenisBerkases;

    $jenisPermintaanDoks[] = $temp;
}
// Yii::$app->d->ddx($jenisPermintaanDoks);

$this->registerJs(
    'vm.$data.jenisPermintaanDoks = vm.$data.jenisPermintaanDoks.concat(' . json_encode($jenisPermintaanDoks) . ');'.
    'vm.$data.permohonan.id_jenis_permintaan_dok = ' . json_encode($model['permohonan']->id_jenis_permintaan_dok) . ';'.
    'vm.$data.permohonan.permohonanDokumens = vm.$data.permohonan.permohonanDokumens.concat(' . json_encode($permohonanDokumens) . ');',
    // 'vm.$data.permohonan.permohonanDokumens = Object.assign({}, vm.$data.permohonan.permohonanDokumens, ' . json_encode($permohonanDokumens) . ');',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['permohonan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['permohonan'], ['class' => '']);
}

if (isset($model['permohonan_dokumen'])) foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen) {
    if ($permohonanDokumen->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($permohonanDokumen, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?= $this->render('/layouts/page-header', [
    'title' => $title,
    'icon' => null,
    'backLink' => 'site/index',
]) ?>

<div class="container padding-y-30">

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nomor_permohonan'] ?></label>
        <?= Html::activeHiddenInput($model['permohonan'], 'nomor_permohonan'); ?>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nomor_permohonan ?></p>
    </div>

    <div class="form-group">
        <label class="control-label">NIK</label>
        <p class="form-control-static border-bottom"><?= Yii::$app->user->identity->pemohon->nik ?></p>
    </div>

    <div class="form-group">
        <label class="control-label">Nama</label>
        <p class="form-control-static border-bottom"><?= Yii::$app->user->identity->pemohon->nama ?></p>
    </div>

    <div class="form-group">
        <label class="control-label">No HP</label>
        <p class="form-control-static border-bottom"><?= Yii::$app->user->identity->pemohon->nomor_hp ?></p>
    </div>

    <?= $form->field($model['permohonan'], 'keperluan')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'keperluan', ['class' => 'control-label', 'label' => 'Catatan']); ?>
        <?= Html::activeTextArea($model['permohonan'], 'keperluan', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'keperluan', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'keperluan')->end(); ?>

    <?= $form->field($model['permohonan'], 'id_jenis_permintaan_dok')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'id_jenis_permintaan_dok', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['permohonan'], 'id_jenis_permintaan_dok', ArrayHelper::map(\app_hackptsa_admin\models\JenisPermintaanDok::find()->indexBy('id')->asArray()->all(), 'id', 'jenis_permintaan_dok'), ['prompt' => 'Choose one please', 'class' => 'form-control', 'v-model' => 'permohonan.id_jenis_permintaan_dok']); ?>
        <?= Html::error($model['permohonan'], 'id_jenis_permintaan_dok', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'id_jenis_permintaan_dok')->end(); ?>


    <template v-for="(value, key, index) in jenisPermintaanDoks">
        <template v-for="(v, k, i) in value.jenisBerkases">
            <template v-if="value.id == permohonan.id_jenis_permintaan_dok">
                <input type="hidden" v-bind:id="'permohonandokumen-' + k + '-id'" v-bind:name="'PermohonanDokumen[' + k + '][id]'" class="form-control" type="text">
                <input type="hidden" v-bind:id="'permohonandokumen-' + k + '-id_jenis_berkas'" v-bind:name="'PermohonanDokumen[' + k + '][id_jenis_berkas]'" class="form-control" type="text" v-bind:value="v.id">
                <div v-bind:class="'form-group field-permohonandokumen-' + k + '-virtual_nama_file_upload'">
                    <label v-bind:for="'permohonandokumen-' + k + '-virtual_nama_file_upload'" class="control-label">{{v.nama_berkas}}</label>
                    <!-- <input v-bind:id="'permohonandokumen-' + k + '-virtual_nama_file_upload'" v-bind:name="'PermohonanDokumen[' + k + '][virtual_nama_file_upload]'" class="form-control" type="text"> -->
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"><a v-bind:href="v.virtual_nama_file_download">{{v.file}}</a></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input v-bind:id="'permohonandokumen-' + k + '-virtual_nama_file_upload'" v-bind:name="'PermohonanDokumen[' + k + '][virtual_nama_file_upload]'" aria-invalid="false" type="file">
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                    <div class="help-block"></div>
                </div>
            </template>
        </template>
    </template>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

</div>