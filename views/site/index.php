<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('@web/app/site/list-index-konsultasi.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/site/list-index-dokumen.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="has-bg-img">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/cover.jpeg" class="bg-img" style="width: 100%; height: auto;">
    <!-- <img src="../technoart/asset/img/10-azure.png" class="bg-img" style="width: 100%; height: auto;"> -->
    <div class="container <?= Yii::$app->user->isGuest ? 'padding-y-200' : 'padding-y-150' ?> m-padding-y-60 text-lightest">
        <div class="margin-bottom-50">
            <span class="padding-20 fs-40 m-fs-26 bg-dark"><?= Yii::$app->params['app.name'] ?></span>
        </div>
        <div>
            <span class="padding-20 fs-20 m-fs-13 bg-dark"><?= Yii::$app->params['app.description'] ?></span>
        </div>
    </div>
</div>

<?php if (!Yii::$app->user->isGuest) : ?>

    <div class="padding-y-30 border-bottom">
        <div class="container padding-y-30">
            <div class="text-center fs-24 text-dark margin-bottom-30">
                Pilih Jenis Pelayanan
            </div>
            <div class="box box-break-sm box-space-lg equal">
                <a class="box-6 underline-none shadow bg-orange padding-x-30 padding-y-20 border-orange hover-bg-lightest hover-text-orange" href="<?= Yii::$app->urlManager->createUrl("permohonan/konsultasi") ?>">
                    <div class="box">
                        <div class="box-2">
                            <div class="fs-50"><i class="fa fa-paper-plane"></i></div>
                        </div>
                        <div class="box-10">
                            <div class="fs-28">
                                Konsultasi
                            </div>
                            <div class="fs-14">
                                Klik disini untuk konsultasi dengan petugas
                            </div>
                        </div>
                    </div>
                </a>
                <a class="box-6 underline-none shadow bg-azure padding-x-30 padding-y-20 border-azure hover-bg-lightest hover-text-azure" href="<?= Yii::$app->urlManager->createUrl("permohonan/dokumen") ?>">
                    <div class="box">
                        <div class="box-2">
                            <div class="fs-50"><i class="fa fa-envelope"></i></div>
                        </div>
                        <div class="box-10">
                            <div class="fs-28">
                                Pengurusan Dokumen
                            </div>
                            <div class="fs-14">
                                Klik disini untuk mengajukan permohonan dokumen
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="padding-y-30">
        <div class="container padding-y-30">
            <div class="margin-bottom-30">
                <div class="padding-y-5 margin-bottom-15">
                    <span class="border-bottom fs-16 text-rose ff-default f-italic">Daftar Konsultasi</span>
                </div>
                <table id="konsultasi" class="datatables display nowrap table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Permohonan</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal Permohonan</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">NIK</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Nama</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Keperluan</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Komentar</th>
                        </tr>
                        <tr class="dt-search">
                            <th class="padding-0"></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nomor permohonan" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tanggal permohonan" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nik" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nama" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search keperluan" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search komentar" class="form-control border-none f-normal padding-x-5"/></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="margin-bottom-30">
                <div class="padding-y-5 margin-bottom-15">
                    <span class="border-bottom fs-16 text-rose ff-default f-italic">Daftar Pengurusan Dokumen</span>
                </div>
                <table id="dokumen" class="datatables display nowrap table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Permohonan</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Jenis Permintaan Dokumen</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal Permohonan</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">NIK</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Nama</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Keperluan</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Komentar</th>
                            <th class="text-dark f-normal" style="border-bottom: 1px">Estimasi Selesai</th>
                        </tr>
                        <tr class="dt-search">
                            <th class="padding-0"></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nomor permohonan" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search jenis permintaan dokumen" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tanggal permohonan" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nik" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nama" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search keperluan" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search komentar" class="form-control border-none f-normal padding-x-5"/></th>
                            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search estimasi selesai" class="form-control border-none f-normal padding-x-5"/></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

<?php endif; ?>