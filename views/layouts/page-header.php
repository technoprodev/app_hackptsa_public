<div class="border-bottom">
    <div class="container padding-y-15 fs-18">
    	<?php if (isset($backLink)) : ?>
        	<a href="<?= Yii::$app->urlManager->createUrl($backLink) ?>"><i class="fa fa-arrow-left margin-right-20 text-azure"></i></a>
    	<?php else: ?>
        	<i class="fa <?= $icon ?> margin-right-20"></i>
    	<?php endif; ?>
        <span class="text-darkest"><?= $title ?></span>
    </div>
</div>