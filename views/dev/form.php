<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);

//
$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['dev']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');',
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['dev']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['dev'], ['class' => '']);
}

if ($model['dev_extend']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['dev_extend'], ['class' => '']);
}

if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <label class="control-label"><?= $model['dev']->attributeLabels()['id_combination'] ?></label>
        <?= Html::activeHiddenInput($model['dev'], 'id_combination', ['initial-value ng-model' => 'dev.id_combination']); ?>
        <p class="form-control-static border-bottom"><?= $model['dev']->id_combination ?></p>
    </div>

    <?= $form->field($model['dev'], 'id_combination')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_combination', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_combination', ['class' => 'form-control', 'maxlength' => true, 'disabled' => true]); ?>
        <?= Html::error($model['dev'], 'id_combination', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'id_combination')->end(); ?>

    <?= $form->field($model['dev'], 'id_combination')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_combination', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_combination', ['class' => 'form-control', 'maxlength' => true, 'readonly' => true]); ?>
        <?= Html::error($model['dev'], 'id_combination', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'id_combination')->end(); ?>

    <?= $form->field($model['dev'], 'id_user_defined'/*, ['enableAjaxValidation' => true]*/)->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_user_defined', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_user_defined', ['class' => 'form-control', 'maxlength' => true, 'initial-value ng-model' => 'dev.id_user_defined']); ?>
        <?= Html::error($model['dev'], 'id_user_defined', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'id_user_defined')->end(); ?>

    <?= $form->field($model['dev'], 'text')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'text', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'dev.text']); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'control-label']); ?>
        <?= Html::activePasswordInput($model['dev'], 'text', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'text', ['class' => 'form-control', 'maxlength' => true, 'list' => 'list-dev-autocomplete']); ?>
        <datalist id="list-dev-autocomplete">
            <?php
            $options = ArrayHelper::map(\app_hackptsa_public\models\DevCategoryOption::find()->asArray()->all(), 'id', 'name');
            foreach ($options as $key => $value) { ?>
                <option value="<?= $key ?>"><?= $value ?></option>
            <?php } ?>
        </datalist>
        <?= Html::error($model['dev'], 'text', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['dev'], 'text', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <hr>

    <?= $form->field($model['dev'], 'link', ['options' => ['class' => 'form-group radio-elegant']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'link', ['class' => 'control-label']); ?>
        <?= Html::activeRadioList($model['dev'], 'link', ArrayHelper::map(\app_hackptsa_public\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'row row-radio', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
                return "<div class='radio col-xs-3'><label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label></div>";

                // equal to this
                /*$disabled = in_array($value, ['val1', 'val2', '1']) ? true : false;

                $radio = Html::radio($name, $checked, ['value' => $value, 'checked' => $checked, 'disabled' => $disabled]);
                return Html::tag('div', Html::label($radio . '<i></i>' . $label), ['class' => 'radio col-xs-3']);*/
            }]); ?>
        <?= Html::error($model['dev'], 'link', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'link')->end(); ?>

    <?= $form->field($model['dev'], 'enum')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'enum', ['class' => 'control-label']); ?>
        <?= Html::activeRadioList($model['dev'], 'enum', $model['dev']->getEnum('enum'), ['unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
                return "<div class='radio'><label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label></div>";
            }]); ?>
        <?= Html::error($model['dev'], 'enum', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'enum')->end(); ?>

    <?= $form->field($model['dev'], 'virtual_category', ['options' => ['class' => 'form-group checkbox-elegant']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'virtual_category', ['class' => 'control-label']); ?>
        <?= Html::activeCheckboxList($model['dev'], 'virtual_category', ArrayHelper::map(\app_hackptsa_public\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'row row-checkbox', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
                return "<div class='checkbox col-xs-3'><label><input type='checkbox' name='$name' value='$value' $checked $disabled v-model='dev.virtual_category'><i></i>$label</label></div>";
            },
        ]); ?>
        <?= Html::error($model['dev'], 'virtual_category', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'virtual_category')->end(); ?>

    <?= $form->field($model['dev'], 'set', ['options' => ['class' => 'form-group checkbox-fancy']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'set', ['class' => 'control-label']); ?>
        <?= Html::activeCheckboxList($model['dev'], 'set', $model['dev']->getSet('set'), ['unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, ['val1', 'val2', 'technology']) ? 'disabled' : '';
                return "<div class='checkbox'><label><input type='checkbox' name='$name' value='$value' $checked $disabled><i></i>$label</label></div>";
            }
        ]); ?>
        <?= Html::error($model['dev'], 'set', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'set')->end(); ?>

    
    <?= $form->field($model['dev'], 'checklist')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'checklist', ['class' => 'control-label']); ?>
        <div class="checkbox">
            <?= Html::activeCheckbox($model['dev'], 'checklist', ['uncheck' => 0]); ?>
        </div>
        <?= Html::error($model['dev'], 'checklist', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'checklist')->end(); ?>
    

    <hr>

    <!--
    $form->field($model['dev'], 'link')->begin(); ?>
        Html::activeLabel($model['dev'], 'link', ['class' => 'control-label']); ?>
        Html::activeDropDownList($model['dev'], 'link', ArrayHelper::map(\app_hackptsa_public\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name', 'parent'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        Html::error($model['dev'], 'link', ['class' => 'help-block']); ?>
    $form->field($model['dev'], 'link')->end(); ?>

    
    $form->field($model['dev'], 'virtual_category')->begin(); ?>
        Html::activeLabel($model['dev'], 'virtual_category', ['class' => 'control-label']); ?>
        Html::activeListBox($model['dev'], 'virtual_category', ArrayHelper::map(\app_hackptsa_public\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'form-control', 'multiple' => true, 'unselect' => null, 'v-model' => 'dev.virtual_category']); ?>
        Html::error($model['dev'], 'virtual_category', ['class' => 'help-block']); ?>
    $form->field($model['dev'], 'virtual_category')->end(); ?>
    

    $form->field($model['dev'], 'enum')->begin(); ?>
        Html::activeLabel($model['dev'], 'enum', ['class' => 'control-label']); ?>
        Html::activeDropDownList($model['dev'], 'enum', $model['dev']->getEnum('enum'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        Html::error($model['dev'], 'enum', ['class' => 'help-block']); ?>
    $form->field($model['dev'], 'enum')->end(); ?>

    $form->field($model['dev'], 'set')->begin(); ?>
        Html::activeLabel($model['dev'], 'set', ['class' => 'control-label']); ?>
        Html::activeListBox($model['dev'], 'set', $model['dev']->getSet('set'), ['class' => 'form-control', 'multiple' => true, 'unselect' => null]); ?>
        Html::error($model['dev'], 'set', ['class' => 'help-block']); ?>
    $form->field($model['dev'], 'set')->end(); ?>

    <hr>
    -->

    <?= $form->field($model['dev'], 'file')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'file', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'file', ['class' => 'form-control']); ?>
        <?= Html::error($model['dev'], 'file', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'file')->end(); ?>
    
    <hr>

    <?= $form->field($model['dev_extend'], 'extend')->begin(); ?>
        <?= Html::activeLabel($model['dev_extend'], 'extend', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev_extend'], 'extend', ['class' => 'form-control', 'maxlength' => true, 'initial-value ng-model' => 'dev.textinput']); ?>
        <?= Html::error($model['dev_extend'], 'extend', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev_extend'], 'extend')->end(); ?>

    <hr>

    <!-- 
    <?php if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $value): ?>
        <?= $form->field($model['dev_child'][$key], "[$key]child")->begin(); ?>
            <?php /*Html::activeLabel($model['dev_child'][$key], "[$key]child", ['class' => 'control-label']);*/ ?>
            <?php /*Html::activeTextInput($model['dev_child'][$key], "[$key]child", ['class' => 'form-control', 'maxlength' => true]);*/ ?>
            <?php /*Html::error($model['dev_child'][$key], "[$key]child", ['class' => 'help-block']);*/ ?>
        <?= $form->field($model['dev_child'][$key], "[$key]child")->end(); ?>
    <?php endforeach; ?>
    -->

    <template v-if="typeof dev.devChildren == 'object'">
        <template v-for="(value, key, index) in dev.devChildren">
            <div v-show="!(value.id < 0)">
                <input type="hidden" v-bind:id="'devchild-' + key + '-id'" v-bind:name="'DevChild[' + key + '][id]'" class="form-control" type="text" v-model="dev.devChildren[key].id">
                <div v-bind:class="'form-group field-devchild-' + key + '-child'">
                    <label v-bind:for="'devchild-' + key + '-child'" class="control-label">Child</label>
                    <div class="input-group">
                        <input v-bind:id="'devchild-' + key + '-child'" v-bind:name="'DevChild[' + key + '][child]'" class="form-control" type="text" v-model="dev.devChildren[key].child">
                        <div v-on:click="removeChild(key)" class="input-group-addon bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                    </div>
                    <div class="help-block"></div>
                </div>
            </div>
        </template>
    </template>

    <a v-on:click="addChild" class="btn btn-default">Add Child</a>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['dev']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?>
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>