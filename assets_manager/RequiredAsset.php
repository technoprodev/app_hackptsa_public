<?php
namespace app_hackptsa_public\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_hackptsa_public/assets';

    public $css = [];

    public $js = [
        'js/app.js',
    ];

    public $depends = [
        'technosmart\assets_manager\VueAsset',
        'technosmart\assets_manager\VueResourceAsset',
        'technosmart\assets_manager\VueDefaultValueAsset',
        'technosmart\assets_manager\RequiredAsset',
    ];
}