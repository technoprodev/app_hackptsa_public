<?php
$params['user.passwordResetTokenExpire'] = 3600;
$params['slider'] = false;

$config = [
    'id' => 'app_hackptsa_public',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_hackptsa_public\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_hackptsa_public',
        ],
        'user' => [
            'identityClass' => 'app_hackptsa_public\models\UserIdentity',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_hackptsa_public', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_hackptsa_public',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '' => 'site/index',

                '<alias:contact-us|signup|confirm|login|logout|error>' => 'site/<alias>',
                '<controller:\b(?!\b(site)\b)[\w\-]+>/<id:[\d]+>' => '<controller>/index',
                '<controller:\b(?!\b(site)\b)[\w\-]+>' => '<controller>/index',

                '<controller:\b(?!\b(site)\b)[\w\-]+>/<action:\b(?!\b(index)\b)[\w\-]+>/<id:[\d]+>' => '<controller>/<action>',
                '<controller:\b(?!\b(site)\b)[\w\-]+>/<action:\b(?!\b(index)\b)[\w\-]+>' => '<controller>/<action>',

                '<url:.+/>' => 'site/redirect-slash',
            ],
            // <word: -> cuma nangkap yang sebelum tanda tanya
            // '<controller:[\w\-]+>/<action:[\w\-]+>/<<param1:[\w\-]+>:[a-zA-Z0-9_\-\/]+>' => '<controller>/<action>?param1',
        ],
    ],
    'params' => $params,
];

return $config;