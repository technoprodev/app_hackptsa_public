<?php
namespace app_hackptsa_public\controllers;

use Yii;
use app_hackptsa_public\models\Dev;
use app_hackptsa_public\models\DevExtend;
use app_hackptsa_public\models\DevChild;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * DevController implements an highly advanced CRUD actions for Dev model.
 */
class DevController extends \technosmart\yii\web\Controller
{
    public function actionDatatables()
    {
        $db = Dev::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('dev d')
                ->join('LEFT JOIN', 'dev_category_option dco', 'dco.id = d.link')
                ->join('LEFT JOIN', 'dev_extend de', 'de.id_dev = d.id');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'd.id',
                'd.id_combination',
                'd.id_user_defined',
                'dco.name AS link',
                'de.extend AS extend',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionIndex($id = null)
    {
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Datas',
            ]);
        }

        $model['dev'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of ' . $model['dev']->id_combination,
        ]);
    }

    public function actionCreate()
    {
        $render = false;

        $model['dev'] = isset($id) ? $this->findModel($id) : new Dev();
        $model['dev_extend'] = isset($model['dev']->devExtend) ? $model['dev']->devExtend : new DevExtend();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['dev']->load($post);
            $model['dev_extend']->load($post);
            if (isset($post['DevChild'])) {
                foreach ($post['DevChild'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $devChild = $this->findModelChild($value['id']);
                        $devChild->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $devChild = $this->findModelChild(($value['id']*-1));
                        $devChild->isDeleted = true;
                    } else {
                        $devChild = new DevChild();
                        $devChild->setAttributes($value);
                    }
                    $model['dev_child'][] = $devChild;
                }
            }

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['dev']),
                    ActiveForm::validate($model['dev_extend'])
                );
                return $this->json($result);
            }

            $transaction['dev'] = Dev::getDb()->beginTransaction();

            try {
                if ($model['dev']->isNewRecord) {}
                if (!$model['dev']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $model['dev_extend']->id_dev = $model['dev']->id;
                if (!$model['dev_extend']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                $error = false;
                if (isset($model['dev_child']) and is_array($model['dev_child'])) {
                    foreach ($model['dev_child'] as $key => $devChild) {
                        if ($devChild->isDeleted) {
                            if (!$devChild->delete()) {
                                $error = true;
                            }
                        } else {
                            $devChild->id_dev = $model['dev']->id;
                            if (!$devChild->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                
                if ($error) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['dev']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['dev']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['dev']->rollBack();
            }
        } else {
            foreach ($model['dev']->devChildren as $key => $devChild)
                $model['dev_child'][] = $devChild;

            if ($model['dev']->isNewRecord) {
                $model['dev']->id_combination = $this->sequence('dev-id_combination');
                $model['dev_child'][] = new DevChild();
            }

            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Create New Data',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['dev']->id]);
    }

    public function actionUpdate($id)
    {
        $render = false;

        $model['dev'] = isset($id) ? $this->findModel($id) : new Dev();
        $model['dev_extend'] = isset($model['dev']->devExtend) ? $model['dev']->devExtend : new DevExtend();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['dev']->load($post);
            $model['dev_extend']->load($post);
            if (isset($post['DevChild'])) {
                foreach ($post['DevChild'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $devChild = $this->findModelChild($value['id']);
                        $devChild->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $devChild = $this->findModelChild(($value['id']*-1));
                        $devChild->isDeleted = true;
                    } else {
                        $devChild = new DevChild();
                        $devChild->setAttributes($value);
                    }
                    $model['dev_child'][] = $devChild;
                }
            }

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['dev']),
                    ActiveForm::validate($model['dev_extend'])
                );
                return $this->json($result);
            }

            $transaction['dev'] = Dev::getDb()->beginTransaction();

            try {
                if ($model['dev']->isNewRecord) {}
                if (!$model['dev']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $model['dev_extend']->id_dev = $model['dev']->id;
                if (!$model['dev_extend']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                if (isset($model['dev_child']) and is_array($model['dev_child'])) {
                    foreach ($model['dev_child'] as $key => $devChild) {
                        if ($devChild->isDeleted) {
                            if (!$devChild->delete()) {
                                throw new \yii\base\UserException('Data tidak berhasil dihapus.');
                            }
                        } else {
                            $devChild->id_dev = $model['dev']->id;
                            if (!$devChild->save()) {
                                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                            }
                        }
                    }
                }
                
                $transaction['dev']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['dev']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['dev']->rollBack();
            }
        } else {
            foreach ($model['dev']->devChildren as $key => $devChild)
                $model['dev_child'][] = $devChild;

            if ($model['dev']->isNewRecord) {
                $model['dev']->id_combination = $this->sequence('dev-id_combination');
                $model['dev_child'][] = new DevChild();
            }

            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Data ' . $model['dev']->id_combination,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['dev']->id]);
    }

    public function actionTest()
    {
        // curl -X POST
        // --header "Content-Type: application/x-www-form-urlencoded"
        // --header "Accept: application/json"
        // --header "Authorization: Bearer ac9f53bc09ea8ace87e3b3c9d48420d4"
        // -d "msisdn=081221689238&content=test%20saja" "https://api.mainapi.net/smsnotification/1.0.0/messages"

        $body = array(
            "msisdn" => "081221689238",
            "content" => "test saja",
        );
        $xml_post_string =  http_build_query($body);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.mainapi.net/smsnotification/1.0.0/messages');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
            'Authorization: Bearer ac9f53bc09ea8ace87e3b3c9d48420d4',
            'Content-Length: ' . strlen($xml_post_string)
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);

        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result=curl_exec($ch);
        curl_close($ch);

        return $this->json(json_decode($result));
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Dev::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelChild($id)
    {
        if (($model = DevChild::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    ////

    protected function arrayDiff($current = [], $new = [])
    {
        /*$update_diff = [];
        $insert_diff = array_diff_ukey($new, $current, function($x, $y) use ($current, $new, &$update_diff) {
            if ($x == $y) {
                if ($current[$x] != $new[$x]) {
                    $update_diff[] = $new[$x];
                }

                return 0;
            }
            else return $x > $y ? 1 : -1;
        });*/
        $update_diff = count($new) > 0 ? array_intersect_key($new, $current) : [];
        $insert_diff = count($new) > 0 ? array_diff_key($new, $current) : [];
        $delete_diff = count($current) > 0 ? array_diff_key($current, $new) : [];

        return [$insert_diff, $update_diff, $delete_diff];
    }

    public function actionArrayDiff()
    {
        $current = [
            '0a' => 'nol',
            '1a' => 'satu',
            '2a' => 'dua',
            '3a' => 'tiga',
        ];

        $new = [
            '1a' => 'satu',
            '2a' => 'two',
            '3a' => 'three',
            '4a' => 'four',
        ];
        
        // Yii::$app->debug->debugx($this->arrayDiff($current, $new));

        $query = new \yii\db\Query();
        $query
            ->select('*')
            ->from('dev');
        $query->orWhere(['id'=>'12', 'id_user_defined'=>'syiwa']);
        $query->andWhere('1=1');
        $a = '';
        $query->andFilterWhere(['or', ['like', 'id', 12], ['like', 'id', $a]]);

        $result = $query->createCommand()->sql;
        Yii::$app->debug->debugx($result);
        Yii::$app->debug->debugx($query->all());
    }
}