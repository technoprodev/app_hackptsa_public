<?php
namespace app_hackptsa_public\controllers;

use Yii;
use app_hackptsa_admin\models\Pemohon;
use app_hackptsa_admin\models\Permohonan;
use app_hackptsa_admin\models\PermohonanDokumen;
use app_hackptsa_admin\models\Mediator;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

class PermohonanController extends \technosmart\yii\web\Controller
{
    protected function findModelKonsultasi($nomor_permohonan)
    {
        if (($model = Permohonan::findOne(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'konsultasi'])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelDokumen($nomor_permohonan)
    {
        if (($model = Permohonan::findOne(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'dokumen'])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDokumen()
    {
        // Yii::$app->d->ddx(Yii::$app->user->identity->id);
        $render = false;

        $model['permohonan'] = new Permohonan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['permohonan']->load($post);
            if (isset($post['PermohonanDokumen'])) {
                foreach ($post['PermohonanDokumen'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $permohonanDokumen = $this->findModelPermohonanDokumen($value['id']);
                        $permohonanDokumen->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $permohonanDokumen = $this->findModelPermohonanDokumen(($value['id']*-1));
                        $permohonanDokumen->isDeleted = true;
                    } else {
                        $permohonanDokumen = new PermohonanDokumen();
                        $permohonanDokumen->setAttributes($value);
                    }
                    // Yii::$app->d->dd(UploadedFile::getInstance($permohonanDokumen, "[$key]virtual_nama_file_upload"));

                    // $temp = UploadedFile::getInstance($permohonanDokumen, "[$key]virtual_nama_file_upload");
                    // $name = $permohonanDokumen->virtual_nama_file_upload->name;
                    // $permohonanDokumen->nama_file = $name;
                    // $permohonanDokumen->nama_file = $temp->name;
                    // $permohonanDokumen->virtual_nama_file_upload = $temp;
                    /*Yii::$app->d->dd($permohonanDokumen->nama_file);
                    Yii::$app->d->dd($permohonanDokumen->virtual_nama_file_upload);
                    Yii::$app->d->ddx($permohonanDokumen);*/

                    /*$uploadRoot = Yii::$app->params['configurations_file']['permohonan_dokumen-nama_file']['alias_upload_root'];
                    $path = Yii::getAlias($uploadRoot) . '/' . $permohonanDokumen->id;
                    if ( !is_dir($path) ) mkdir($path);
                    UploadedFile::getInstance($permohonanDokumen, "[$key]virtual_nama_file_upload")->saveAs($path . '/' . 'asdf');*/
                    
                    $permohonanDokumen->nama_file="dami dulu";
                    $model['permohonan_dokumen'][] = $permohonanDokumen;
                }
            }
            // Yii::$app->d->ddx($model['permohonan_dokumen']);
            // Yii::$app->d->ddx($_FILES);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['permohonan'])
                );
                return $this->json($result);
            }

            $transaction['permohonan'] = Permohonan::getDb()->beginTransaction();

            try {
                $model['permohonan']->id_pemohon = Yii::$app->user->identity->id;
                $model['permohonan']->jenis_permohonan = 'dokumen';
                $model['permohonan']->status_dokumen = 'diajukan';
                 $model['permohonan']->komentar = '_';

                $model['permohonan']->nik = Yii::$app->user->identity->pemohon->nik;
                $model['permohonan']->nomor_hp = Yii::$app->user->identity->pemohon->nomor_hp;
                $model['permohonan']->nama = Yii::$app->user->identity->pemohon->nama;
                $model['permohonan']->email = Yii::$app->user->identity->pemohon->email;
                $model['permohonan']->alamat = Yii::$app->user->identity->pemohon->alamat;
                if (!$model['permohonan']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                $error = false;
                if (isset($model['permohonan_dokumen']) and is_array($model['permohonan_dokumen'])) {
                    foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen) {
                        if ($permohonanDokumen->isDeleted) {
                            if (!$permohonanDokumen->delete()) {
                                $error = true;
                            }
                        } else {
                            $permohonanDokumen->id_permohonan = $model['permohonan']->id;
                            if (!$permohonanDokumen->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                
                if ($error) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['permohonan']->commit();
                Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Permohonan Anda sedang diproses dengan No Permohonan " . $model['permohonan']->nomor_permohonan . "");
                Yii::$app->session->setFlash('success', 'Permohonan berhasil dikirim. Tunggu konfirmasi selanjutnya dari kami. Terima kasih.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            }
        } else {
            foreach ($model['permohonan']->permohonanDokumens as $key => $permohonanDokumen)
                $model['permohonan_dokumen'][] = $permohonanDokumen;

            if ($model['permohonan']->isNewRecord) {
                $model['permohonan']->nomor_permohonan = $this->sequence('permohonan-nomor_permohonan');
                $model['permohonan']->scenario = 'form-create';
            }

            $render = true;
        }

        if ($render)
            return $this->render('form-dokumen', [
                'model' => $model,
                'title' => 'Input Pengurusan Dokumen',
            ]);
        else
            return $this->redirect(['site/index']);
    }

	public function actionKonsultasi()
    {
        // Yii::$app->d->ddx(Yii::$app->user->identity->id);
        $render = false;

        $model['permohonan'] = new Permohonan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['permohonan']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['permohonan'])
                );
                return $this->json($result);
            }

            $transaction['permohonan'] = Permohonan::getDb()->beginTransaction();

            try {
                $model['permohonan']->id_pemohon = Yii::$app->user->identity->id;
                $model['permohonan']->komentar = '_';
                $model['permohonan']->jenis_permohonan = 'konsultasi';

                $model['permohonan']->nik = Yii::$app->user->identity->pemohon->nik;
                $model['permohonan']->nomor_hp = Yii::$app->user->identity->pemohon->nomor_hp;
                $model['permohonan']->nama = Yii::$app->user->identity->pemohon->nama;
                $model['permohonan']->email = Yii::$app->user->identity->pemohon->email;
                $model['permohonan']->alamat = Yii::$app->user->identity->pemohon->alamat;

                if (!$model['permohonan']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['permohonan']->commit();
                Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Permohonan Anda telah dikirim ke petugas dengan No Permohonan " . $model['permohonan']->nomor_permohonan . ". Harap menunggu respon petugas selanjutnya.");
                Yii::$app->session->setFlash('success', 'Permohonan berhasil dikirim. Tunggu tanggapan selanjutnya dari kami. Terima kasih.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            }
        } else {
            foreach ($model['permohonan']->permohonanDokumens as $key => $permohonanDokumen)
                $model['permohonan_dokumen'][] = $permohonanDokumen;

            if ($model['permohonan']->isNewRecord) {
                $model['permohonan']->nomor_permohonan = $this->sequence('permohonan-nomor_permohonan');
                $model['permohonan']->scenario = 'form-create';
            }

            $render = true;
        }

        if ($render)
            return $this->render('form-konsultasi', [
                'model' => $model,
                'title' => 'Input Konsultasi',
            ]);
        else
            return $this->redirect(['site/index']);
    }

    public function actionViewDokumen($nomor_permohonan)
    {
        $model['permohonan'] = $this->findModelDokumen($nomor_permohonan);

        return $this->render('one-dokumen', [
            'model' => $model,
            'title' => 'Riwayat Dokumen ' . $model['permohonan']->nomor_permohonan,
        ]);
    }

    public function actionViewKonsultasi($nomor_permohonan)
    {
        $model['permohonan'] = $this->findModelKonsultasi($nomor_permohonan);

        return $this->render('one-konsultasi', [
            'model' => $model,
            'title' => 'Riwayat Konsultasi ' . $model['permohonan']->nomor_permohonan,
        ]);
    }
}