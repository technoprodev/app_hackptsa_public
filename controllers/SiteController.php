<?php
namespace app_hackptsa_public\controllers;

use Yii;
use technosmart\controllers\SiteController as SiteControl;
use app_hackptsa_public\models\Login;
use app_hackptsa_public\models\Signup;
use app_hackptsa_public\models\Confirm;
use app_hackptsa_public\models\UserIdentity;
use app_hackptsa_admin\models\Pemohon;

class SiteController extends SiteControl
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $render = false;

        $model['signup'] = new Signup();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['signup']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['signup'])
                );
                return $this->json($result);
            }

            $transaction['db'] = \technosmart\models\User::getDb()->beginTransaction();
            $transaction['dba'] = \app_hackptsa_admin\models\User::getDb()->beginTransaction();

            try {
                if (!$model['signup']->signup()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

    			$this->smsOtp($model['signup']->nomor_hp);

                $transaction['db']->commit();
                $transaction['dba']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['db']->rollBack();
                $transaction['dba']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['db']->rollBack();
                $transaction['dba']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render) {
            $this->layout = 'empty';
            return $this->render('signup', [
                'model' => $model,
                'title' => 'Signup',
            ]);
        }
        else {
            return $this->redirect(['confirm', 'hp' => $model['signup']->nomor_hp]);
        }
    }

    public function actionConfirm($hp)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $render = false;

        $model['confirm'] = new Confirm();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['confirm']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['confirm'])
                );
                return $this->json($result);
            }

            try {
                if (!$this->confirmOtp($hp, $model['confirm']->pin)) {
                    throw new \yii\base\UserException('Verifikasi gagal. Harap lakukan verifikasi ulang.');
                }

                $pemohon = Pemohon::findOne(['nomor_hp' => $hp]);
                $pemohon->is_verified = 1;
                $pemohon->save();

                $user = UserIdentity::findOne(['id' => $pemohon->id]);
                $user->status = 1;
                $user->save();
                // Yii::$app->d->ddx($pemohon);

                if (!Yii::$app->getUser()->login($user)) {
                    throw new \yii\base\UserException('Verifikasi gagal. Harap lakukan verifikasi ulang.');
                }

                Yii::$app->session->setFlash('success', 'Registrasi berhasil. Selamat datang di aplikasi PTSA');
            } catch (\Exception $e) {
                $render = true;
            } catch (\Throwable $e) {
                $render = true;
            }
        } else {
            $render = true;
        }

        if ($render) {
            $this->layout = 'empty';
            return $this->render('confirm', [
                'model' => $model,
                'title' => 'Confirm',
            ]);
        }
        else {
            return $this->goBack();
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $model['login'] = new Login();
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'empty';
            return $this->render('login', [
                'model' => $model,
                'title' => 'Login',
            ]);
        }
    }

    public function smsOtp($nomor_hp)
    {
        // return true;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.mainapi.net/smsotp/1.0.1/otp/hackptsa$nomor_hp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => "phoneNum=$nomor_hp&digit=4",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "authorization: Bearer 40448fb4ca42d987606ddae2fd70a96c",
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: e1d8b832-c203-17e6-c712-72520b731824"
            ),
            CURLOPT_CAINFO => 'C:\\xampp\\htdocs\\technosmart\\app_hackptsa_loket\\other/cacert.pem',
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return true;
        }
    }

    public function confirmOtp($nomor_hp, $pin)
    {
        // return true;
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.mainapi.net/smsotp/1.0.1/otp/hackptsa$nomor_hp/verifications",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "otpstr=$pin&digit=4",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "authorization: Bearer 40448fb4ca42d987606ddae2fd70a96c",
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: 155bedba-b480-400b-78c0-a6f02cf63a59"
            ),
            CURLOPT_CAINFO => 'C:\\xampp\\htdocs\\technosmart\\app_hackptsa_loket\\other/cacert.pem',
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return true;
        }
    }
}