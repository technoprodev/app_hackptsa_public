<?php
namespace app_hackptsa_public\controllers;

use Yii;
use app_hackptsa_public\models\UserIdentity;
use app_hackptsa_public\models\User;
use technosmart\controllers\UserController as UserControl;

class UserController extends UserControl
{
    public function actionDatatables()
    {
        $db = UserIdentity::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('user u')
                ->join('INNER JOIN', 'hackptsa.user uu', 'uu.id = u.id');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'u.id',
                'u.name',
                'u.username',
                'u.email',
                'u.status',
                'uu.username AS same_username',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionCreate()
    {
        $render = false;

        $model['userIdentity'] = isset($id) ? $this->findModel($id) : new UserIdentity();
        $model['userIdentity']->scenario = 'repass';
        $model['user'] = isset($model['userIdentity']->user) ? $model['userIdentity']->user : new User();
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['userIdentity']->load($post);
            $model['user']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['userIdentity']),
                    ActiveForm::validate($model['user'])
                );
                return $this->json($result);
            }

            $transaction = UserIdentity::getDb()->beginTransaction();
            $childTransaction = User::getDb()->beginTransaction();

            try {
                if ($model['userIdentity']->isNewRecord) {
                    $model['userIdentity']->status = 1;
                }
                if (!$model['userIdentity']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $model['user']->id = $model['userIdentity']->id;
                $model['user']->username = $model['userIdentity']->username;
                if (!$model['user']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                Yii::$app->authManager->revokeAll($model['userIdentity']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['userIdentity']->id);
                }

                $transaction->commit();
                $childTransaction->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
                $childTransaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
                $childTransaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Create New User',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['userIdentity']->id]);
    }

    public function actionUpdate($id)
    {
        $render = false;

        $model['userIdentity'] = isset($id) ? $this->findModel($id) : new UserIdentity();
        $model['user'] = isset($model['userIdentity']->user) ? $model['userIdentity']->user : new User();
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['userIdentity']->load($post);
            $model['user']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['userIdentity']),
                    ActiveForm::validate($model['user'])
                );
                return $this->json($result);
            }

            $transaction = UserIdentity::getDb()->beginTransaction();
            $childTransaction = User::getDb()->beginTransaction();

            try {
                if ($model['userIdentity']->isNewRecord) {
                    $model['userIdentity']->status = 1;    
                }
                if (!$model['userIdentity']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $model['user']->id = $model['userIdentity']->id;
                $model['user']->username = $model['userIdentity']->username;
                if (!$model['user']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                Yii::$app->authManager->revokeAll($model['userIdentity']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['userIdentity']->id);
                }

                $transaction->commit();
                $childTransaction->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
                $childTransaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
                $childTransaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Update ' . $model['userIdentity']->name . ' (username: ' . $model['userIdentity']->username . ')',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['userIdentity']->id]);
    }

    public function actionResetPassword($id)
    {
        $render = false;

        $model['userIdentity'] = isset($id) ? $this->findModel($id) : new UserIdentity();
        $model['userIdentity']->scenario = 'repass';
        $model['user'] = isset($model['userIdentity']->user) ? $model['userIdentity']->user : new User();
        
        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $key => $value)
            $roles[$key] = $value->description;
        $model['assignments'] = isset($id) ? Yii::$app->authManager->getRolesByUser($id) : [];
        foreach ($model['assignments'] as $key => $value)
            $model['assignments'][$key] = $value->name;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['userIdentity']->load($post);
            $model['user']->load($post);
            $model['assignments'] = Yii::$app->request->post('assignments', []);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['userIdentity']),
                    ActiveForm::validate($model['user'])
                );
                return $this->json($result);
            }

            $transaction = UserIdentity::getDb()->beginTransaction();
            $childTransaction = User::getDb()->beginTransaction();

            try {
                if ($model['userIdentity']->isNewRecord) {
                    $model['userIdentity']->status = 1;    
                }
                if (!$model['userIdentity']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $model['user']->id = $model['userIdentity']->id;
                $model['user']->username = $model['userIdentity']->username;
                if (!$model['user']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                Yii::$app->authManager->revokeAll($model['userIdentity']->id);

                foreach ($model['assignments'] as $assignment) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($assignment), $model['userIdentity']->id);
                }

                $transaction->commit();
                $childTransaction->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction->rollBack();
                $childTransaction->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction->rollBack();
                $childTransaction->rollBack();
            }

        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-reset-password', [
                'model' => $model,
                'roles' => $roles,
                'title' => 'Reset Password ' . $model['userIdentity']->name . ' (username: ' . $model['userIdentity']->username . ')',
            ]);

        else
            return $this->redirect(['index', 'id' => $model['userIdentity']->id]);
    }

    protected function findModel($id)
    {
        if (($model = UserIdentity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}